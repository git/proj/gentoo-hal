#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# Copyright (c) 2008 Saleem Abdulrasool <compnerd@compnerd.org>

# Very simple script which converts the InputDevice section for keyboards into a
# FDI rule.  Any time a specific model is present, we ignore it and set it to
# evdev, as that will be the driver used.

import sys
import xf86config
from xf86config import XF86SectionMissing

def xkb_model(value):
   model  = '      <!-- Option "XkbModel" "%s" -->' % (value,) + '\n'
   model += '      <merge key="input.xkb.model" type="string">evdev</merge>'

   return model

def xkb_rules(value):
   return '      <merge key="input.xkb.rules" type="string">%s</merge>' % (value,)

def xkb_layout(value):
   return '      <merge key="input.xkb.layout" type="string">%s</merge>' % (value,)

def xkb_options(values):
   options = '      <merge key="input.xkb.options" type="strlist">%s</merge>' % (values.split(',')[0])
   for value in values.split(',')[1:]:
      options += '\n' + '      <append key="input.xkb.options" type="strlist">%s</append>' % (value,)

   return options

def xkb_variant(value):
   return '      <merge key="input.xkb.variant" type="string">%s</merge>' % (value,)

if __name__ == '__main__':
   OptionHandlers = {
                        'XkbModel' : xkb_model,
                        'XkbRules' : xkb_rules,
                        'XkbLayout' : xkb_layout,
                        'XkbOptions' : xkb_options,
                        'XkbVariant' : xkb_variant,
                    }

   (configuration, path) = xf86config.readConfigFile()

   if not configuration:
      sys.stderr.write("Failed to read xorg.conf\n")
      sys.exit(-1)

   try:
      keyboard = xf86config.getCoreKeyboard(configuration)
   except XF86SectionMissing:
      sys.stderr.write("No CoreKeyboard\n")
      sys.exit(-1)

   rule  = '<?xml version="1.0" encoding="utf-8"?>' + '\n'
   rule += '<deviceinfo version="0.2">' + '\n'
   rule += '   <match key="info.capabilities" contains="input.keys">' + '\n'
   for option in keyboard.options:
      if option.name in OptionHandlers:
         rule += OptionHandlers[option.name](option.val) + '\n'
      else:
         sys.stderr.write('Failed to translate: %s %s\n' % (option.name, option.val))
   rule += '   </match>' + '\n'
   rule += '</deviceinfo>' + '\n'

   print rule

# vim: set ts=3 sw=3 et nowrap:
